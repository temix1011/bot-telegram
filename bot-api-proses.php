<?php

if (!defined('HS')) {
    die('Tidak boleh diakses langsung.');
}


/*
 *  Programmer	: Hasanudin HS
 *  Email     	: banghasan@gmail.com
 *  Telegram  	: @hasanudinhs
 *
 *  Name      	: Template bot telegram - php
 *  Fungsi    	: Sample bot API
 *  Pembuatan 	: Mei 2016
 *
 *  File 	  	: bot-api-proses.php
 *  Tujuan		: semua proses bot ada di sini
 *  ____________________________________________________________
*/



/*

Contoh penggunaan :
~~~~~~~~~~~~~~~~~~~~~

Kirim Aksi
----------
(typing, upload_photo, record_video, upload_video, record_audio, upload_audio, upload_document, find_location) :

    sendApiAction($chatid);
    sendApiAction($chatid, 'upload_photo');


Kirim Pesan :
----------
    sendApiMsg($chatid, 'pesan');
    sendApiMsg($chatid, 'pesan *tebal*', false, 'Markdown');


Kirim Markup Keyboard :
----------
    $keyboard = [
        [ 'tombol 1', 'tombol 2' ],
        [ 'tombol 3', 'tombol 4' ],
        [ 'tombol 5' ]
    ];

    sendApiKeyboard($chatid, 'tombol pilihan', $keyboard);


Kirim Inline Keyboard
----------
    $inkeyboard = [
        [
            ['text'=>'tombol 1', 'callback_data' => 'data 1'],
            ['text'=>'tombol 2', 'callback_data' => 'data 2']
        ],
        [
            ['text'=>'tombol akhir', 'callback_data' => 'data akhir']
        ]
    ];

    sendApiKeyboard($chatid, 'tombol pilihan', $inkeyboard, true);


editMessageText
----------
    editMessageText($chatid, $message_id, $text, $inkeyboard, true);



Menyembunyikan keyboard :
----------
    sendApiHideKeyboard($chatid, 'keyboard off');


kirim sticker
----------

    sendApiSticker($chatid, 'BQADAgADUAADxKtoC8wBeZm11cjsAg')


Dan Lain-lain :-D
~~~~~~~~~~~~~~~~~~~~~

*/


function prosesApiMessage($sumber)
{
    $updateid = $sumber['update_id'];

   // if ($GLOBALS['debug']) mypre($sumber);

    if (isset($sumber['message'])) {
        $message = $sumber['message'];

        if (isset($message['text'])) {
            prosesPesanTeks($message);
        } elseif (isset($message['sticker'])) {
            prosesPesanSticker($message);
        } else {
            // gak di proses silakan dikembangkan sendiri
        }
    }

    if (isset($sumber['callback_query'])) {
        prosesCallBackQuery($sumber['callback_query']);
    }

    return $updateid;
}

function prosesPesanSticker($message)
{
    // if ($GLOBALS['debug']) mypre($message);
}

function prosesCallBackQuery($message)
{
    // if ($GLOBALS['debug']) mypre($message);

    $message_id = $message['message']['message_id'];
    $chatid = $message['message']['chat']['id'];
    $data = $message['data'];

    //mac
    $test = array('02-02-02-02-02-02','02-03-03-03-03-03','01-02-01-02-01-02','C4-42-02-96-AA-40','02-FB-02-9C-72-15','00-08-22-E8-02-FC','02-24-BE-B0-E7-5C','00-EC-0A-12-02-37','02-00-01-02-03-04','08-74-02-CB-A5-29','02-AB-23-07-20-01','70-4D-7B-55-DC-02','02-BB-AE-16-00-82','02-00-50-AA-3A-33','02-F4-A0-0F-1C-D9','02-E748-59-8B-03','02-00-00-00-00-00','02-17-B0-02-3A-90','78-02-F8-3A-75-A1','02-39-FF-D6-20-30','02-90-6F-57-07-D4','08-8C-2C-02-50-77');
    shuffle($test);
    $ran = array_shift($test);
    $tebal = '**';
    $mac = "### Mac Generate ###\n".$tebal."$ran".$tebal."\n#################";

    //kampus
    // $list = array('825881465@ut.ac.id | 2015112091967','836251871@ut.ac.id | 2015206051970', '819823324@ut.ac.id | 2009205101991', '824954571@ut.ac.id | 2014102081989');
    // // $data2 = $test[array_rand($list)];
    // shuffle($list);
    // $data3 = explode(' | ', $list);
    // $us = $data3[0];
    // $pass = $data3[1];
    // $kampus = "### Generate Kampus ### \nUsername = $us\nPassword = $pass";
    // $kam = array_shift($kampus);

    $inkeyboard = [
        [
            ['text' => 'Generate Mac', 'callback_data' => $mac],
            ['text' => 'Generate Kampus', 'callback_data' => 'Coming Soon'],
        ],
        // [
        //     ['text' => 'keyboard inline', 'callback_data' => '!inline'],
        // ],
        [
            ['text' => 'All Fitur', 'callback_data' => '!fitur'],
        ],
    ];

    // $text = '*'.date('H:i:s').'* data baru : '.$data;
    $text = '`'.$data.'`';

    editMessageText($chatid, $message_id, $text, $inkeyboard, true);

    $messageupdate = $message['message'];
    $messageupdate['text'] = $data;

    prosesPesanTeks($messageupdate);
}


function prosesPesanTeks($message)
{
    // if ($GLOBALS['debug']) mypre($message);

    $pesan = $message['text'];
    $chatid = $message['chat']['id'];
    $fromid = $message['from']['id'];

        // variable penampung nama user
    isset($message["from"]["last_name"]) 
    ? $namakedua = $message["from"]["last_name"] 
    : $namakedua = '';   
    $namauser = $message["from"]["first_name"]. ' ' .$namakedua;


    switch (true) {

        case $pesan == '/start':
        sendApiAction($chatid);
        $spasi = "\n";
        $text = "Hi $namauser, kenalin Aku Sinta :) Aku hanyalah sebuah Bot yang dibuat oleh @XTeMixX karena kelaman jombs...Tugas aku hanyalah sebagai asisten Managemen Aplikasi Mantools \n".$spasi.$spasi."Kata yang saya mengerti".$spasi."- !id".$spasi."- !fitur".$spasi."- !donasi (Example : !donasi 02-xx-xx-xx-xx-xx)".$spasi."- !update".$spasi."- !hide";
        sendApiMsg($chatid, $text);
        break;

        case $pesan == '!id':
        $namauser = $message["from"]["first_name"];
        sendApiAction($chatid);
        $text = 'Hello '."$namauser ".'ID Kamu adalah: '.$fromid;
        sendApiMsg($chatid, $text);
        break;

        case $pesan == '!update':
        sendApiAction($chatid);
        $text = "### Versi Terkini ### \nVersi Aplikasi : 2.1 \nTanggal Rilis : 09/04/2019 \nLink Download : https://mantools-id.com";
        sendApiMsg($chatid, $text);
        break;    

        case $pesan == '!fitur':
        sendApiAction($chatid);
        $keyboard = [
            // [
            //     ['text' => 'Create UT', 'callback_data' => 'Coming Soon'],
            // ]
            ['Create UT','Create Trisakti'],
            ['Fitur Generate'],
        ];
        sendApiKeyboard($chatid, 'tombol pilihan', $keyboard);
        break;

        case $pesan == 'Fitur Generate':
        sendApiAction($chatid);
        $inline = [
            [
                ['text' => 'Ya', 'callback_data' => 'Welcome To Fitur Generate Mac & Wifi.id Komunitas Kampus'],
            ]
        ];
        sendApiKeyboard($chatid, '!!! INFORMASI !!! 
            Tidak Semua Akun Komunitas Kampus Atau Mac Address Autoconnect Yang Di Berikan 100% WORK. Dengan Menulis Ya/Klik Button Ya Di Bawah Ini Bahwa Anda Mengerti', $inline, true);
        break;

        case $pesan == 'Welcome To Fitur Generate Mac & Wifi.id Komunitas Kampus':
        sendApiAction($chatid);
        sendApiHideKeyboard($chatid, 'keyboard off');
        break;

        // case preg_match("/\!encode_base64 (.*)/", $pesan, $hasil):
        // sendApiAction($chatid);

        // // $text = 'Dari '.$namauser.' '.$hasil[1];
        // $spasi = "\n";
        // $text = base64_encode($hasil[1]);
        // sendApiMsg($chatid, $text, false, 'Markdown');
        // break;

        case preg_match("/\!donasi (.*)/", $pesan, $hasil):
        sendApiAction($chatid);

        // $text = 'Dari '.$namauser.' '.$hasil[1];
        $spasi = "\n";
        $text = "Dari : $namauser".$spasi."id User : `".$chatid.$spasi."`Pesan :`".$hasil[1]."`";
        $terimakasi = "Terimakasi $namauser donasi anda telah terkirim ke XTeMix";
        sendApiMsg("337363393", $text, false, 'Markdown');
        sendApiMsg($chatid, $terimakasi, false, 'Markdown');
        break;

//tanggapan
        // case preg_match("/\/ta (.*)/", $pesan, $hasil):
        // sendApiAction($chatid);

        // // $text = 'Dari '.$namauser.' '.$hasil[1];
        // $spasi = "\n";
        // $text = "Dari : $namauser".$spasi."id User : '*".$chatid.$spasi."*Pesan : '".$hasil[1];
        // $terimakasi = "Terimakasi $namauser donasi anda telah terkirim ke XTeMix";
        // // sendApiMsg("337363393", $text, false, 'Markdown');
        // $user=$hasil[1];
        // sendApiMsg($user, $terimakasi, false, 'Markdown');
        // break;


        default:
        // $spasi = "\n";
        // $bukacode = "<code>";
        // $tutupcode = "</code>"; 
        // $text = '*Maaf kata yang anda katakan saya tidak mengerti*'.$spasi.$spasi.'Kata yang saya mengerti'.$spasi.'- !id'.$spasi.'- !update'.$spasi.'- !keyboard';
        // sendApiMsg($chatid, $text, false, 'Markdown');
        break;
    }
}
